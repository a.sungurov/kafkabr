import 'dart:async';
import 'dart:convert';

import 'kafkabr.dart';

class KafkaHelper {
  final String kafkaUrl;
  int? kafkaPort;

  late ContactPoint _contactPoint;

  KafkaSession? _kafkaSession;

  Producer? _producer;

  Fetcher? _fetcher;

  KafkaHelper({
    required this.kafkaUrl,
    int? port,
  }) : kafkaPort = port ?? 9092 {
    _contactPoint = ContactPoint(kafkaUrl, kafkaPort!);
  }

  Future<String> getMessageByKey(
      {required String topicName,
      int? partitionId,
      required String key,
      int? maxBatchSize,
      Duration? timeout}) async {
    List<Map<String, String>> result;
    _kafkaSession = KafkaSession([_contactPoint]);
    List<Map<String, String>> messagesList = [];

    var group = ConsumerGroup(_kafkaSession!, 'consumerGroupName');
    var topicsList = <String, Set<int>>{
      topicName: <int>{0},
    };

    await group.resetOffsetsToEarliest(topicsList);

    var consumer = Consumer(_kafkaSession!, group, topicsList, 100, 1);

    await for (BatchEnvelope batch
        in consumer.batchConsume(maxBatchSize ?? 20).timeout(
      timeout ?? const Duration(seconds: 3),
      onTimeout: (sink) {
        sink.close();
      },
    )) {
      batch.items.forEach((MessageEnvelope envelope) {
        String messageKey = String.fromCharCodes(envelope.message.key!);

        var foundedMessage = messagesList.firstWhere(
            (element) => element.keys.contains(messageKey),
            orElse: () => {});
        if (foundedMessage.isEmpty) {
          messagesList.add({
            messageKey: String.fromCharCodes(envelope.message.value),
          });
        } else {
          foundedMessage[messageKey] =
              String.fromCharCodes(envelope.message.value);
        }
      });
      batch.commit('metadata');
      if (messagesList.any((element) => element.containsKey(key))) {
        break;
      }
    }

    await _kafkaSession?.close();

    result = messagesList
        .where((element) => element.keys.any((keyIn) => keyIn == key))
        .toList();

    if (result.isNotEmpty) {
      return result.last[key] ?? '';
    } else {
      return '';
    }
  }

  Future<List<Map<String, String>>> getAllMessagesByConsumer({
    required String topicName,
    int? partitionId,
    int? maxBatchSize,
    Duration? timeout,
    bool resetOffset = false,
  }) async {
    _kafkaSession = KafkaSession([_contactPoint]);
    List<Map<String, String>> messagesList = [];

    var group = ConsumerGroup(_kafkaSession!, 'consumerGroupName');
    var topicsList = <String, Set<int>>{
      topicName: <int>{0},
    };

    if (resetOffset) {
      await group.resetOffsetsToEarliest(topicsList);
    }

    var consumer = Consumer(_kafkaSession!, group, topicsList, 100, 1);

    await for (BatchEnvelope batch
        in consumer.batchConsume(maxBatchSize ?? 20).timeout(
      timeout ?? const Duration(seconds: 3),
      onTimeout: (sink) {
        sink.close();
      },
    )) {
      batch.items.forEach((MessageEnvelope envelope) {
        String messageKey = String.fromCharCodes(envelope.message.key!);
        messagesList.add({
          messageKey: String.fromCharCodes(envelope.message.value),
        });
      });
      batch.commit('metadata');
    }

    await _kafkaSession?.close();

    return messagesList;
  }

  Stream<MessageEnvelope> getStream({
    required String topicName,
    int? partitionId,
    int? offset,
  }) {
    _kafkaSession = KafkaSession([_contactPoint]);
    _fetcher = Fetcher(_kafkaSession!,
        [TopicOffset(topicName, partitionId ?? 0, offset ?? 0)]);

    return _fetcher!.fetch();
  }

  Future<void> closeSession() async {
    await _kafkaSession?.close();
  }

  Future<List<Map<String, String>>> getAllMessages({
    required String topicName,
    int? partitionId,
    int? maxBatchSize,
    Duration? timeout,
    int? offset,
  }) async {
    _kafkaSession = KafkaSession([_contactPoint]);
    List<Map<String, String>> messagesList = [];

    _fetcher = Fetcher(_kafkaSession!,
        [TopicOffset(topicName, partitionId ?? 0, offset ?? 0)]);
    await for (MessageEnvelope envelope in _fetcher!.fetch().timeout(
      timeout ?? const Duration(seconds: 3),
      onTimeout: (sink) {
        sink.close();
      },
    )) {
      String messageKey = String.fromCharCodes(envelope.message.key!);
      messagesList.add({
        messageKey: String.fromCharCodes(envelope.message.value),
      });
    }
    await _kafkaSession?.close();
    return messagesList;
  }

  Future<String> getLatestMessage({
    required String topicName,
    int? partitionId,
    int? offset,
  }) async {
    _kafkaSession = KafkaSession([_contactPoint]);
    List<String> messagesList = [];

    var offsetMaster = OffsetMaster(_kafkaSession!);
    var listOfLatest = await offsetMaster.fetchLatest(<String, Set<int>>{
      topicName: <int>{partitionId ?? 0},
    });
    offset = listOfLatest.first.offset;

    _fetcher = Fetcher(_kafkaSession!,
        [TopicOffset(topicName, partitionId ?? 0, offset ?? 0)]);
    await for (MessageEnvelope envelope in _fetcher!.fetch()) {
      String message = String.fromCharCodes(envelope.message.value);
      messagesList.add(message);
    }
    await _kafkaSession?.close();
    return messagesList.first;
  }

  Future<bool> postMessage({
    required String topicName,
    required int partitionId,
    required String key,
    required String value,
    bool encodeWithUTF8 = false,
  }) async {
    _kafkaSession = KafkaSession([_contactPoint]);
    _producer = Producer(_kafkaSession!, 1, 1000);

    var postResult = await _producer?.produce(
      [
        ProduceEnvelope(
          topicName,
          partitionId,
          [
            Message(
              encodeWithUTF8 ? utf8.encode(value) : value.codeUnits,
              key: encodeWithUTF8 ? utf8.encode(key) : key.codeUnits,
            ),
          ],
        ),
      ],
    );
    await _kafkaSession?.close();
    return postResult?.hasErrors ?? false;
  }
}
